-- Please see enus.lua for reference.

QuestHelper_Translations.bgBG =
 {
  -- Displayed by locale chooser.
  LOCALE_NAME = "Български/Bulgarian",
  
  -- Messages used when starting.
  LOCALE_ERROR = nil,
  ZONE_LAYOUT_ERROR = "Имаш стара версия на Quest Helper. За да може да работи правилно отиди на http://www.questhelp.us и свали новата версия. В момента ти използваш версия %1",
  HOME_NOT_KNOWN = "Твоят дом не е познат. Когато имаш възможност, говори с Innkeeper.",
  PRIVATE_SERVER = "QuestHelper не се поддържа от пиратски сървъри.",
  PLEASE_RESTART = "Имаше проблем при стартирането на QuestHelper. Излез от играта напълно и след това влез наново. Ако проблемът все още съществува, преинсталирай QuestHelper-a",
  NOT_UNZIPPED_CORRECTLY = "QuestHelper беше инсталиран неправилно. Предлагаме да използваш Curse клиента за да инсталираш. Свали от %h(http://www.questhelp.us).",
  PLEASE_SUBMIT = "%h(Споделял ли си данни с QuestHelper напоследък?) QuestHelper разчита на данните споделени от потребители. Ако имаш няколко свободни минути, отиди на главната страница на QuestHelper %h (http://www.questhelp.us) и следвай инструкциите. Благодаря!",
  HOW_TO_CONFIGURE = nil,
  TIME_TO_UPDATE = "Може би има %h(нова версия на Quest Helper) за сваляне. Новите версии съдържат нови куестове и много оправени бъгове. Моля ъпдейтни!",
  
  -- Route related text.
  ROUTES_CHANGED = "Пътищата за летене на героят ти бяха променени.",
  HOME_CHANGED = "Твоят дом беше сменен.",
  TALK_TO_FLIGHT_MASTER = "Говори с местния Flight Master",
  TALK_TO_FLIGHT_MASTER_COMPLETE = "Благодаря.",
  WILL_RESET_PATH = "Ще опресни информация за пътя.",
  UPDATING_ROUTE = "Опресни маршрута.",
  
  -- Special tracker text
  QH_LOADING = "QuestHelper зарежда (%1)",
  QH_FLIGHTPATH = "Изчисляване на пътищата за летене (%1%) ...",
  QH_RECALCULATING = "Изчиславане на пътя (%1%)",
  QUESTS_HIDDEN_1 = "Може да има скрити куестове",
  QUESTS_HIDDEN_2 = "(десен клик за списък)",
  
  -- Locale switcher.
  LOCALE_LIST_BEGIN = "Налични езици:",
  LOCALE_CHANGED = "Езикът сменен на: %h1",
  LOCALE_UNKNOWN = "Езикът %h1 е непознат.",
  
  -- Words used for objectives.
  SLAY_VERB = "Убий",
  ACQUIRE_VERB = "Събери",
  
  OBJECTIVE_REASON = "%1 %h2 за куест %h3.", -- %1 is a verb, %2 is a noun (item or monster)
  OBJECTIVE_REASON_FALLBACK = "%h1 за куест %h2",
  OBJECTIVE_REASON_TURNIN = "Върни куест %h1.",
  OBJECTIVE_PURCHASE = "Поръчай от %h1",
  OBJECTIVE_TALK = "Говори със %h1.",
  OBJECTIVE_SLAY = "Убий %h1.",
  OBJECTIVE_LOOT = "Плячкосай %h1.",
  OBJECTIVE_OPEN = "Отвори %h1",
  
  OBJECTIVE_MONSTER_UNKNOWN = "непознато чудовище",
  OBJECTIVE_ITEM_UNKNOWN = "непознат предмет",
  
  ZONE_BORDER_SIMPLE = "%1 граница",
  
  -- Stuff used in objective menus.
  PRIORITY = "Приоритет",
  PRIORITY1 = "Най-голям",
  PRIORITY2 = "Голям",
  PRIORITY3 = "Нормален",
  PRIORITY4 = "Нисък",
  PRIORITY5 = "Най-нисък",
  SHARING = "Споделяне",
  SHARING_ENABLE = "Сподели",
  SHARING_DISABLE = "Не споделяй",
  IGNORE = "Игнорирай",
  IGNORE_LOCATION = "Игнорирай това място.",
  
  IGNORED_PRIORITY_TITLE = "Избрания приоритет ще бъде игнориран.",
  IGNORED_PRIORITY_FIX = nil,
  IGNORED_PRIORITY_IGNORE = "Сам ще слагам приоритети.",
  
  -- "/qh find"
  RESULTS_TITLE = "Резултати от търсенето",
  NO_RESULTS = "Няма никакви!",
  CREATED_OBJ = "Създаден: %1",
  REMOVED_OBJ = "Премахнат: %1",
  USER_OBJ = nil,
  UNKNOWN_OBJ = "Незнам къде трябва да отидеш, за да изпълниш задачата.",
  INACCESSIBLE_OBJ = nil,
  FIND_REMOVE = nil,
  FIND_NOT_READY = "QuestHelper не е заредил все още. Изчакай минута и опитай отново",
  FIND_CUSTOM_LOCATION = nil,
  FIND_USAGE = "\"Намери\" не работи ако не му кажеш какво да намери. Напиши %h(/qh help) за инструкции.",
  
  -- Shared objectives.
  PEER_TURNIN = "Изчакай %h1 да върне куеста %h2",
  PEER_LOCATION = "Помогни на %h1 да достигне място в %h2",
  PEER_ITEM = "Помогни на %1 за събере %h2",
  PEER_OTHER = "Помогни на %1 с %h2",
  
  PEER_NEWER = "%h1 използва по нова версия на аддона. Ти също трябва да го направиш.",
  PEER_OLDER = "%h1 използва по-стара версия",
  
  UNKNOWN_MESSAGE = "Непознат тип съобщение \"%1\" от \"%2\".",
  
  -- Hidden objectives.
  HIDDEN_TITLE = "Скрити куестове",
  HIDDEN_NONE = "Няма куестове, които са скрити",
  DEPENDS_ON_SINGLE = "Зависи от \"%1\"",
  DEPENDS_ON_COUNT = "Зависи от %1 скрити куеста",
  DEPENDS_ON = "Зависи от филтрираните куестове",
  FILTERED_LEVEL = "Скрит, заради нивото",
  FILTERED_GROUP = "Скрит, защото куеста изисква по-голяма група",
  FILTERED_ZONE = "Скрит, заради зона",
  FILTERED_COMPLETE = "Филтриран поради завършеност",
  FILTERED_BLOCKED = "Скрит поради незавършен по-ранен куест",
  FILTERED_UNWATCHED = "Филтриран, защото куеста не се следи в куест лога (L)",
  FILTERED_WINTERGRASP = "Скрит, защото това е PvP куест от Wintergrasp",
  FILTERED_RAID = "Скрит, защото неможе да се изпълни докато си в рейд група",
  FILTERED_USER = "Ти поиска този куест да бъде скрит",
  FILTERED_UNKNOWN = "Не знам как да го завърша",
  
  HIDDEN_SHOW = "Покажи",
  HIDDEN_SHOW_NO = "Непоказваем",
  HIDDEN_EXCEPTION = "Добави изключение",
  DISABLE_FILTER = "Изключи филтър: %1",
  FILTER_DONE = "завършен",
  FILTER_ZONE = "зона",
  FILTER_LEVEL = "ниво",
  FILTER_BLOCKED = "блокиран",
  FILTER_WATCHED = "наблюдаван",
  
  -- Achievements. Or, as they are known in the biz, "cheeves".
  -- God I hate the biz.
  ACHIEVEMENT_CHECKBOX = "Маркирай за да добавиш тозъ achievement към QuestHelper",
  
  -- Nagging. (This is incomplete, only translating strings for the non-verbose version of the nag command that appears at startup.)
  NAG_MULTIPLE_NEW = "Имаш %h(нова информация) за %h1, и %h(ъпдейтната информация) за %h2.",
  NAG_SINGLE_NEW = "Ти имаш %h(нова информация) за %h1",
  NAG_ADDITIONAL = nil,
  NAG_POLLUTED = "Твоята база от данни е била замърсена от пиратски сървър. Ще бъде почистена при следващо стартиране.",
  
  NAG_NOT_NEW = "Все още нямаш информация в базата от данни.",
  NAG_NEW = "Обмисли да споделиш данните, които имаш, за да могат другите да имат полза също.",
  NAG_INSTRUCTIONS = "Напиши %h(/qh submit) за инструкции как да ни изпратиш данни",
  
  NAG_SINGLE_FP = nil,
  NAG_SINGLE_QUEST = "куест",
  NAG_SINGLE_ROUTE = "път за летене",
  NAG_SINGLE_ITEM_OBJ = "куест с предмет",
  NAG_SINGLE_OBJECT_OBJ = nil,
  NAG_SINGLE_MONSTER_OBJ = "куест със създание",
  NAG_SINGLE_EVENT_OBJ = nil,
  NAG_SINGLE_REPUTATION_OBJ = "куест с репутация",
  NAG_SINGLE_PLAYER_OBJ = nil,
  
  NAG_MULTIPLE_FP = "%1 flight masters",
  NAG_MULTIPLE_QUEST = "%1 куеста",
  NAG_MULTIPLE_ROUTE = "%1 пътища за летене",
  NAG_MULTIPLE_ITEM_OBJ = "%1 куестове с предмети",
  NAG_MULTIPLE_OBJECT_OBJ = nil,
  NAG_MULTIPLE_MONSTER_OBJ = "%1 куестове с чудовища",
  NAG_MULTIPLE_EVENT_OBJ = nil,
  NAG_MULTIPLE_REPUTATION_OBJ = "%1 куестове с репутация",
  NAG_MULTIPLE_PLAYER_OBJ = "%1 куестове с играчи",
  
  -- Stuff used by dodads.
  PEER_PROGRESS = "Прогресът на %1:",
  TRAVEL_ESTIMATE = "Време за пътуване:",
  TRAVEL_ESTIMATE_VALUE = "%t1",
  WAYPOINT_REASON = "Посети %h1 по пътя за:",
  FLIGHT_POINT = nil,

  -- QuestHelper Map Button
  QH_BUTTON_TEXT = "QuestHelper",
  QH_BUTTON_TOOLTIP1 = "Ляв клик: %1 информация за пътя",
  QH_BUTTON_TOOLTIP2 = "Десен клик: покажи менюто на настройките.",
  QH_BUTTON_SHOW = "Покажи",
  QH_BUTTON_HIDE = "Скрий",

  MENU_CLOSE = "Затвори менюто",
  MENU_SETTINGS = "Настройки",
  MENU_ENABLE = "Позволи",
  MENU_DISABLE = "Изключи",
  MENU_OBJECTIVE_TIPS = "%1 подсказки за куестове",
  MENU_TRACKER_OPTIONS = "Куест тракер",
  MENU_QUEST_TRACKER = "%1 куест тракер",
  MENU_TRACKER_LEVEL = "%1 ниво на куеста",
  MENU_TRACKER_QCOLOUR = "%1 цвят на трудността на куеста",
  MENU_TRACKER_OCOLOUR = "%1 цвят на куеста",
  MENU_TRACKER_SCALE = "Големина на тракера",
  MENU_TRACKER_RESET = "Върни позицията в начално положение",
  MENU_FLIGHT_TIMER = "%1 таймер за летене",
  MENU_ANT_TRAILS = "%1 точки по картата",
  MENU_WAYPOINT_ARROW = "%1 стрелка, която показва пътя",
  MENU_MAP_BUTTON = "%1 бутон на картата",
  MENU_ZONE_FILTER = "%1 филтър: зона",
  MENU_DONE_FILTER = "%1 филтър за завършеност",
  MENU_BLOCKED_FILTER = "%1 блокиран филтър",
  MENU_WATCHED_FILTER = "%1 наблюдаван филтър",
  MENU_LEVEL_FILTER = "%1 филтър за ниво",
  MENU_LEVEL_OFFSET = "Филтър: level, промяна",
  MENU_ICON_SCALE = "Големина на иконата",
  MENU_FILTERS = "Филтри",
  MENU_PERFORMANCE = nil,
  MENU_LOCALE = "Език",
  MENU_PARTY = "Група",
  MENU_PARTY_SHARE = "%1 споделяне на куестове",
  MENU_PARTY_SOLO = "%1 игнорирай група",
  MENU_HELP = "Помощ",
  MENU_HELP_SLASH = "Команди",
  MENU_HELP_CHANGES = "Промени",
  MENU_HELP_SUBMIT = "Споделяне на данни",
  
  -- Added to tooltips of items/npcs that are watched by QuestHelper but don't have any progress information.
  -- Otherwise, the PEER_PROGRESS text is added to the tooltip instead.
  TOOLTIP_WATCHED = "Следен от QuestHelper",
  TOOLTIP_QUEST = "За куест %h1",
  TOOLTIP_PURCHASE = "Поръчай %h1",
  TOOLTIP_SLAY = "Убий за %h1",
  TOOLTIP_LOOT = "Плячкосай за %h1.",
  TOOLTIP_OPEN = "Отвори за %h1",
  
  -- Settings
  SETTINGS_ARROWLINK_ON = "Ще използва %h1, за да показва куестове.",
  SETTINGS_ARROWLINK_OFF = "Няма да използва %h1, за да показва куестове",
  SETTINGS_ARROWLINK_ARROW = "QuestHelper стрелка",
  SETTINGS_ARROWLINK_CART = nil,
  SETTINGS_ARROWLINK_TOMTOM = "ТомТом",
  SETTINGS_PRECACHE_ON = nil,
  SETTINGS_PRECACHE_OFF = nil,
  
  SETTINGS_MENU_ENABLE = "Позволи",
  SETTINGS_MENU_DISABLE = "Забрани",
  SETTINGS_MENU_CARTWP = "%1 Стрелка на Картографер",
  SETTINGS_MENU_TOMTOM = "%1 стрелка ТомТом",
  
  SETTINGS_MENU_ARROW_LOCK = "Заключи",
  SETTINGS_MENU_ARROW_ARROWSCALE = "Големина на стрелката",
  SETTINGS_MENU_ARROW_TEXTSCALE = "Големина на текста",
  SETTINGS_MENU_ARROW_RESET = "Върни към обичайното положение.",
  
  SETTINGS_MENU_INCOMPLETE = "Незавършени куестове",
  
  SETTINGS_RADAR_ON = nil,
  SETTINGS_RADAR_OFF = nil,
  
  -- I'm just tossing miscellaneous stuff down here
  DISTANCE_YARDS = "%h1 ярда",
  DISTANCE_METRES = "%h1 метра"
 }

